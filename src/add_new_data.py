#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Created : 05/07/2022
Modified : 06/07/2022

@author: Scalzitti Nicolas 
"""
import pandas as pd
import sys

def usage():
	print("-- USAGE -- ")
	print("python add_new_data.py -h")
	print("		Print this message")
	print("python add_new_data.py  <path_data>  <ss_type>  <dataset>  <file>  <dataset_type>\n")
	print("<path_data> : path where the raw data are stored")
	print("<ss_type> : type of splice site, choose between 'donor' or 'acceptor'")
	print("<dataset> : Number of the dataset where to add new data [1,2 or 3]")
	print("<file> : file containing the new sequence to added")
	print("<dataset_type> : dataset type to where add new sequences, choose between 'pos' or 'neg' if you want to add data in positive(pos) or negative(neg) set")
	print("\nThe 5 arguments are necessary")
	exit()

def check_length(sequence):
	"""
	Check if the length of the new sequence is 600

	sequene: the input raw sequence
	"""
	if len(sequence) != 600:
		print("[WARNING] The length of the sequence doesn't correspond to 600")

def add_sequence_from_file(path_data, pos_file, neg_file, file, dataset_type, sep="\t"):
	"""
	Add sequences from a csv files. It must contain at least these four columns: 
		Gene_ID, Sequence, Position and Canonical

		Gene_ID: Uniprot_ID for example or other ID.
		Sequence: The raw genomic sequence (if you add a new positiv sequence, 
			      make sure that the dinucleotide (GT or AG (if canonical)) are in the position 301 and 302)
		Position: position of the dinucleotide in the gene.
		Canonical: If true, it is an AG or GT, else, put False.
	file: The file that contains all sequences.
	dataset_type: Choose between 'pos' or 'neg' to select the positive or negative dataset where you will
				  add the new sequence.
	sep: specific separator of the file
	"""
	df = pd.read_csv(file, sep=sep)
	gene_id = df["Gene_ID"]
	sequence = df["Sequence"]
	position = df["Position"]
	canonical = df["Canonical"]
	compteur = 0

	for i in range(len(df)):
		check_length(sequence[i])


		if dataset_type == "pos":
			with open(path_data + pos_file, 'a') as file_R1:
				file_R1.write(f"{gene_id[i]};{sequence[i]};{position[i]};{canonical[i]};External\n")
				compteur += 1
		elif dataset_type == 'neg':
			with open(path_data + neg_file, 'a') as file_R2:
				file_R1.write(f"{gene_id};{sequence[i]};{position[i]};{canonical[i]};External\n")
				compteur += 1
		else:
			print(f"Choose {dataset_type[i]} between 'pos' or 'neg'")
			exit()
	print(f"[INFO] Adding {compteur} new file(s)")

def main():

	sep = "\t" # maybe used ;
	try:
		if sys.argv[1] == "h" or sys.argv[1] == "-h" or sys.argv[1] == "help" or sys.argv[1] == "--help":
			usage() 
		elif len(sys.argv) == 6:
			path_data     = sys.argv[1]
			if path_data[-1] != "/":
				path_data = path_data + "/"
			ss_type       = sys.argv[2]
			dataset       = sys.argv[3]
			file          = sys.argv[4]
			dataset_type  = sys.argv[5]
			pos_file = f"POS_{ss_type}_600.csv"
			neg_file = f"NEG_{dataset}_600_{ss_type}.csv"
			print("[INFO] separator used: \\t")
	
			add_sequence_from_file(path_data, pos_file, neg_file, file, dataset_type, sep=sep)
		else:
			print("[WARNING] Error in the shell command\n")
			usage()
	except:
		usage()


if __name__ == '__main__':
	main()