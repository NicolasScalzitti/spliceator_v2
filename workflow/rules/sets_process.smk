rule building_train_test_set:
	input:
		lab_donor    = expand("data/merged/donor/{dataset}_labels.lab",       dataset=DATASET),
		seq_donor    = expand("data/merged/donor/{dataset}_sequences.seq",    dataset=DATASET),
		lab_acceptor = expand("data/merged/acceptor/{dataset}_labels.lab",    dataset=DATASET),
		seq_acceptor = expand("data/merged/acceptor/{dataset}_sequences.seq", dataset=DATASET)
	output:
		train_donor    = expand("data/training_sets/donor/{dataset}_{size}.seq", dataset=DATASET, size=SIZE),
		train_acceptor = expand("data/training_sets/acceptor/{dataset}_{size}.seq", dataset=DATASET, size=SIZE),
		test_donor     = expand("data/test_sets/donor/{dataset}_{size}.seq", dataset=DATASET, size=SIZE),
		test_acceptor  = expand("data/test_sets/acceptor/{dataset}_{size}.seq", dataset=DATASET, size=SIZE)
	log: 
		l= expand("Logs/{dataset}.log", dataset=DATASET)
	message:
		"Training and test sets creation"
	run:
		# Load data
		for i in range(len(DATASET)):
			add = i * 6
			# DONOR
			sequences = np.loadtxt(input.seq_donor[i], dtype='str') # Load sequences
			labels = np.loadtxt(input.lab_donor[i], dtype='str') # Load labels
			
			# Random spliting of the test and training sets
			x_train, x_test, y_train, y_test = train_test_split(sequences, labels, train_size=0.8)
			
			with open(log.l[i], "a") as f:
				f.write("[" + str(datetime.datetime.now()) + "] - (Donor) train_set size : " + str(len(x_train)) + "\n(Donor) test_set size : " + str(len(x_test)) + "\n")

			for s in range(len(SIZE)):
				utils.write_train_test_sets("donor", x_train, y_train, output.train_donor[s+add], SIZE[s])
				utils.write_train_test_sets("donor", x_test, y_test, output.test_donor[s+add], SIZE[s])
			
			# ACCEPTOR
			sequences = np.loadtxt(input.seq_acceptor[i], dtype='str') # Load sequences
			labels = np.loadtxt(input.lab_acceptor[i], dtype='str') # Load labels

			# # Random spliting of the test and training sets
			x_train, x_test, y_train, y_test = train_test_split(sequences, labels, train_size=0.8)

			for s in range(len(SIZE)):
				utils.write_train_test_sets("acceptor", x_train, y_train, output.train_acceptor[s+add], SIZE[s])
				utils.write_train_test_sets("acceptor", x_test, y_test, output.test_acceptor[s+add], SIZE[s])
		
			with open(log.l[i], "a") as f:
				f.write("[" + str(datetime.datetime.now()) + "] - (Acceptor) train_set size : " + str(len(x_train)) + "\n(Acceptor) test_set size : " + str(len(x_test)) + "\n")

