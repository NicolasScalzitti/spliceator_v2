rule check_integrity:
	input:
		expand("data/raw/{file}.csv", file=INITIAL_FILE)
	output:
		expand("data/raw/{file}.seq", file=INITIAL_FILE) # NPO mettre en temp
	message:
		"Check the integrity of all sequences"
	run:
		for i in range(len(input)):
			rejected_sequences = 0
			all_one_hot_sequences = []
			df = pd.read_csv(input[i], sep=";")
	
			try:
				all_sequences = df["Sequence"]
			except:
				print("The column containing the raw nucleotide sequences should be called 'Sequence'")
				print("The separator in the csv files should be ';'")
				exit()
	
			with open(output[i], "w") as file_W1:
				for i, sequence in enumerate(all_sequences):
					if i == 0:
						pass
					else:
						sequence = sequence.upper()
						if utils.check_dna_integrity(sequence): # Check if the sequence contains only A, C, G or T nuc.
							if sequence in all_one_hot_sequences: # remove redondance
								pass
							else:
								all_one_hot_sequences.append(sequence)
								file_W1.write(sequence + "\n")
						else:
							rejected_sequences+=1

rule merging:
	input:
		pos_donor    = "data/raw/POS_donor_600.seq",
		pos_acceptor = "data/raw/POS_acceptor_600.seq",
		neg_donor    = expand("data/raw/NEG_{dataset}_600_donor.seq", dataset=DATASET),
		neg_acceptor = expand("data/raw/NEG_{dataset}_600_acceptor.seq", dataset=DATASET)
	output:
		lab_donor    = expand("data/merged/donor/{dataset}_labels.lab", dataset=DATASET),
		seq_donor    = expand("data/merged/donor/{dataset}_sequences.seq", dataset=DATASET),
		lab_acceptor = expand("data/merged/acceptor/{dataset}_labels.lab", dataset=DATASET),
		seq_acceptor = expand("data/merged/acceptor/{dataset}_sequences.seq", dataset=DATASET)
	log: 
		l= expand("Logs/{dataset}.log", dataset=DATASET)
	benchmark:
		"benchmarks/benchmark.bch"
	message:
		"Merging data"
	run:
		for i in range(len(DATASET)):
			pos_D, neg_D = utils.load_data(input.pos_donor, hot=True), utils.load_data(input.neg_donor[i], hot=True)
			rejected_donor = set(set(pos_D) & set(neg_D)) # Number of identical sequences in both sets

			pos_A, neg_A = utils.load_data(input.pos_acceptor, hot=True), utils.load_data(input.neg_acceptor[i], hot=True)
			rejected_acceptor = set(set(pos_A) & set(neg_A)) # Number of identical sequences in both sets
			
			utils.write_file(neg_D, rejected_donor, output.lab_donor[i], output.seq_donor[i], 0)
			utils.write_file(pos_D, rejected_donor, output.lab_donor[i], output.seq_donor[i], 1)
	
			utils.write_file(neg_A, rejected_acceptor, output.lab_acceptor[i], output.seq_acceptor[i], 0)
			utils.write_file(pos_A, rejected_acceptor, output.lab_acceptor[i], output.seq_acceptor[i], 1)
			
			with open(log.l[i], "a") as f:
				sys.stderr = sys.stdout = f
				f.write("[" + str(datetime.datetime.now()) + "] - (Donor) Number rejected : " + str(len(rejected_donor)) + "\n(Acceptor) Number rejected : " + str(len(rejected_acceptor)) + "\n")
