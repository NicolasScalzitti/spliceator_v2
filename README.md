<div align="center">
<img src="./data/logo_spliceator.png" align="center" width="300" height="180"> 
<p>Multi-species splice site prediction program using convolutional neural networks</p>

</div>

<center>
![release](https://img.shields.io/badge/Version-2.0-blue.svg)
![license](https://img.shields.io/badge/License-GPLv3-green.svg)
</center>

## Informations

__SPLICEATOR v2.0__ is an eukaryotic splice sites prediction program. It is based on convolutional neural network architecture. A model has been built for each type of splice site (Donor and Acceptor). The strength of __SPLICEATOR__ is based on its multi-species data set (from Human to Protists) which has been carefully established (error-free). We have constructed the positive set (with true splice sites) and the negative set (with counterexamples). Models were trained with 80% of the data and then tested on the remaining 20% of the data.




## Installation

### Create the project 

``` bash
git clone https://gitlab.com/NicolasScalzitti/spliceator_v2.git
cd spliceator_v2
```

### Installing the virtual environment
For better performance, use the provided environment.

``` bash
conda create --name ENV_NAME --file requirements.txt python=3.7
conda activate ENV_NAME
```

or (specific for MAC OSX)

``` bash
conda create --name ENV_NAME python=3.7
conda activate ENV_NAME
conda install pip biopython
PATH/bin/pip install --upgrade tensorflow==2.4.1
```

To get the location of the PATH environment to use the pip package installed in the newly created conda environment, check the instalation of pip with conda and more particularly the section "package plan".

## Launch SPLICEATOR

To see the help:

``` bash
./Spliceator -h
```
...


## Build your model

### Dataset creation
Pour lancer la création des train et test set on utilise le workflow basé sur snakemake

``` bash
conda env create --name sm_splice --file environment.yaml
conda activate sm_splice
```

Lancer le pipeline de création des jeux d'entrainement et de test
``` bash
snakemake --cores NB_CORES
```
NB_CORES = nombre de coeur à utiliser pour le processus

A la fin du processus on obtient deux nouveaux dossier (dans le dossier data): __training_set__ et __test_set__.
Chaque dossier contient 3 différents jeu de données (avec des données différentes).
utilisé les datasets train et test par pair lors du processus d'entrainement et de test


#### Add new raw data
pour ajouter des nouvelles séquences on peu lancer le scripts add_new_data.py

``` bash
python add_new_data.py <path_data>  <ss_type>  <dataset>  <file>  <dataset_type>
```
 
__path_data__ : path where the raw data are stored

__ss_type__ : type of splice site, choose between 'donor' or 'acceptor'

__dataset__ : Number of the dataset where to add new data

__file__ : file containing the new sequence to added

__dataset_type__ : dataset type to where add new sequences, choose between 'pos' or 'neg' if you want to add data in positive(pos) or negative(neg) set

``` bash
# Example:
python add_new_data.py ../data/raw donor 1 ../examples/new_data.csv pos
```

## Build your model

## Installation

## Usage

## Support

## Authors and acknowledgment

## License
