#!/usr/bin/env python
# -*- coding: utf-8 -*-
import numpy as np
import os

def liste_file():
    target_list = []
    p = os.listdir("data/raw")
    for file in p:
        target_list.append(file.replace(".csv", ""))
    return target_list

def one_hot_encoding(sequence):
    """
    Convert a nucleotide sequence in one-hot encoding sequence
    sequence: tha target dna sequence

    return the encoding sequence
    """
    
    sequence = sequence.upper()
    encoded_sequence = ""

    for nuc in sequence:
        if nuc == "A":
            encoded_sequence += "1000"
        elif nuc == "C":
            encoded_sequence += "0100"
        elif nuc == "G":
            encoded_sequence += "0010"
        elif nuc == "T":
            encoded_sequence += "0001"

    return encoded_sequence

def one_hot_decoding(sequence):
    """
    Convert an one-hot encoding sequence to a nucleotide sequence 
    sequence: tha target one-hot sequence

    return the decoding sequence
    """
    decoded_sequence = ""
    tpr = ""

    for nuc in sequence:
        tpr += nuc

        if tpr == "1000":
            decoded_sequence += "A"
            tpr = ""
        if tpr == "0100":
            decoded_sequence += "C"
            tpr = ""
        if tpr == "0010":
            decoded_sequence += "G"
            tpr = ""
        if tpr == "0001":
            decoded_sequence += "T"
            tpr = ""

    return decoded_sequence

def load_data(file, hot=False):
        """
        Load sequence from file

        return a set containing all sequences
        """
        tp_set = set()
        with open(file, "r") as file_R1:
            for line in file_R1:
                if hot:
                    tp_set.add(one_hot_encoding(line.strip()))
                else:
                    tp_set.add(line.strip())
        return tp_set

def check_dna_integrity(dna_sequence, size=600):
    """
    Check the integrity of a dna sequence (contain only ACGT)
    dna_sequence: a target dna sequence 
    size: Length of sequences

    return True or False if the dna sequence is good or not
    """

    list_nuc = ["A", "C", "G", "T"]
    integrity = True

    if len(dna_sequence) != 600:
        integrity = False

    for nuc in dna_sequence:
        if nuc.upper() not in list_nuc:
            integrity = False
    
    return integrity

def write_log(string):
    """
    Write {string} in a log file
    """
    with open("Logs/log_data.log", "a") as f:
        f.write(string + "\n")

def write_file(set_sequence, rejected, out_lab, out_seq, label):
    for seq in set_sequence:
        if seq not in rejected:
            with open(out_lab, "a") as fileWseq1: # Labels donor
                fileWseq1.write(str(label) + "\n")
            with open(out_seq, "a") as fileWseq2: # Sequence donor
                fileWseq2.write(seq + "\n")
                
def write_train_test_sets(ss_type, x_set, y_set, file, size):
    valid_seq = []
    redondance = []
    
    with open(file, "w") as file_W1:
        for x, y in zip(x_set, y_set):
            x = x[ int(1200 - ((size/2) * 4)) : int(1200 + ((size/2) * 4)) ]
            if x in valid_seq: # remove redondance
                redondance.append(x)
            else:   
                valid_seq.append(x)
                file_W1.write(f"{str(x).strip()};{int(y)}\n")

def write_set(x_set, y_set, ss_type, size, filename):
    """
    Save train or test sequence in file

    x_set: Sequences
    y_set: Labels
    filename: Name of the file to save
    """
    if ss_type == "donor":
        path = "data/train/donor/"
    if ss_type == "acceptor":
        path = "data/train/acceptor/"

    with open(path + f"{filename}.seq", "w") as file_W1:
    
        for x, y in zip(x_set, y_set):
            x = x[ int(1200 - ((size/2) *4)) : int(1200 + ((size/2) *4))]
            # a = one_hot_decoding(x)
            file_W1.write(f"{str(x).strip()};{int(y)}\n")